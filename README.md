Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
it shows evaluations of the tests and conditions, etc. for the JUnit tests. This is more appropriate for
logging for the developer to debug and find the flow from rather than output to a user or output to be
able to see the results. 
2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
it comes from JUnit itself, it's a JUnit call that uses the logger since there is only one instance of the logger in the project 
3.  What does Assertions.assertThrows do?
it allows you to test Exceptions within a JUnit test. Multiple Exceptions can be
tested within one JUnit test. 
4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
        the serialization runtime associates a serialVersionUID with each class. An
        InvalidClassException will be thrown if deserialization results in a different
        serialVersionUID. 
    2.  Why do we need to override constructors?
        overriding constructors allows for the subclass to be initialized with the parameters
        given by the constructor, and then it can call its parent class with super using
        the given arguments. 
    3.  Why we did not override other Exception methods?	
        The other Exception methods will automatically be inherited by the TimerException. 
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
    static blocks are executed as soon as the class is loaded in memory. 
6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
    it can then be used with markdown language and can be used to generate the HTML summary at the bottom of projects 

7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
    the Long needed to be initialized to a variable that was initialized as a long. The initialization needed to be
    changed to a long number in timer rather than null.
8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
    the sequence is first checking the input to make sure it is valid, throwing an exception if
    it is not. it will then call the method to call the given amount of time and time it and 
    log how long it should have taken and how long it actually took. 
9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
    
10.  Make a printScreen of your eclipse Maven test run, with console
11.  What category of Exceptions is TimerException and what is NullPointerException
12.  Push the updated/fixed source code to your own repository.